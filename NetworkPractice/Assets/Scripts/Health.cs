﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;


public class Health : NetworkBehaviour {

	//public const int maxHealth = 100;
	public static byte maxHealth = 100;

	[SyncVar (hook = "OnChangeHealth")]
	public byte currentHealth = maxHealth;
	
	public Slider healthSlider;

	public bool destroyOnDeath;

	private NetworkStartPosition[] spawnPoints;

	void Start()
	{
		healthSlider.value = currentHealth;
		if (isLocalPlayer)
		{
			spawnPoints = FindObjectsOfType<NetworkStartPosition>();
		}
	}
	
	public void TakeDamage(byte amount)
	{
		{
			if (!isServer)
				return;
			
			currentHealth -= amount;
			if (currentHealth <= 0)
			{
				if (destroyOnDeath)
				{
					Destroy(gameObject);
				} 
				else
				{
					currentHealth = maxHealth;
					// called on the Server, will be invoked on the Clients
					RpcRespawn();
				}
			}
		}
	}
	
	void OnChangeHealth (byte health)
	{
		healthSlider.value = health;
	}

	[Command]
	void CmdRespawn()
	{
		RpcRespawn ();
	}

	[ClientRpc]
	void RpcRespawn()
	{
		if (isLocalPlayer)
		{
			// move back to zero location
			transform.position = Vector3.zero;
		}
	}
}
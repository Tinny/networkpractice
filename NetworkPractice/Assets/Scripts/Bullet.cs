﻿using UnityEngine;
using System.Collections;


public class Bullet : MonoBehaviour
{
	//private string layerMask;
	private Transform bullerTransform;
	private Rigidbody rigidbody;

	void Start()
	{
		rigidbody = GetComponent<Rigidbody> ();
		bullerTransform = transform;
	}

	void Update()
	{
		rigidbody.velocity = bullerTransform.forward * 20;

		// Destroy the bullet after 2 seconds
		Destroy(gameObject, 2.0f);
	}


	void OnCollisionEnter(Collision col)
	{
		var hit = col.gameObject;
		var health = hit.GetComponent<Health>();
		if (health != null)
		{
			health.TakeDamage (10);
		}
		Destroy (gameObject);
	}
}


// void OnCollisionEnter(Collision col)
	//	{
	//		layerMask = LayerMask.LayerToName (col.gameObject.layer);
	//
	//		switch (layerMask)
	//		{
	//
	//		}
	//	}

﻿using UnityEngine;
using UnityEngine.Networking;

public class EnemySpawner : NetworkBehaviour 
{
	
	public GameObject enemyPrefab;

	public Transform[] enemySpawnPos;

	public byte numberOfEnemies;
	
	public override void OnStartServer()
	{
		for (byte i=0; i < numberOfEnemies; i++)
		{
			var enemy = (GameObject)Instantiate(enemyPrefab, enemySpawnPos[i].position, enemySpawnPos[i].rotation);
			NetworkServer.Spawn(enemy);
		}
	}
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using UnityEngine.Networking;


[RequireComponent (typeof (Animator))]
[RequireComponent (typeof (NavMeshAgent))]


public class NetworkingPlayerController : NetworkBehaviour {
	public static NetworkingPlayerController instRef;
	//Player Reference Variable
	private Transform playerTransform;
	private Animator playerAnimator;
	private NavMeshAgent playerNavmeshAgent;
	private NetworkAnimator networkAnimator;

	//Enemy Reference Variable
	private Transform enemyTransform;
	private LayerMask enemyLayerMask;
	private float distanceFromEnemy;
	private bool isEnemySelected;
	private bool readyToAttack;

	//Common Reference Variable
	private Camera mainCamera;
	private RaycastHit hit;
	private static byte rightMouseClick = 1;
	//private static byte leftMouseClick = 0;

	//SeriallzeField to refer objects to variable and to modify the parameters
	[Header ("Reference Variables")]
	[SerializeField]
	private GameObject projectilePrefab;

	[SerializeField]
	private Transform projectileSpawnPos;

	[Tooltip ("Transform to cast a line from projectile spawn point to this position (Optional)")]
	[SerializeField]
	private Transform rayEndPos;

	[Header ("Character Parameter")]
	[Tooltip ("Velocity to animating the character")]
	[Range (0, 2)]
	[SerializeField]
	private float minRunVelocity;

	[Tooltip ("Number of parameters used in Animator which is attached to this object")]
	[SerializeField]
	private int animatorParameterCount;

	[Tooltip ("Character attack range")]
	[SerializeField]
	private float attackRange;

	[Tooltip ("Character attack delay")]
	[SerializeField]
	private float playerAttackDelay;
	private float attackCoolDown;

	//	[Range(0,500)]
	//	[SerializeField]
	//	private byte maxRayDistance;
	public Transform PlayerTransform 
	{
		get { return playerTransform; }
		set { playerTransform = value; }
	}


	void Awake ()
	{
		instRef = this;
	}
	void Start ()
	{
		attackCoolDown = playerAttackDelay;
		enemyLayerMask = LayerMask.NameToLayer ("Enemy");
		playerNavmeshAgent = GetComponent<NavMeshAgent> ();
		playerAnimator = GetComponent<Animator> ();
		playerTransform = GetComponent<Transform> ();
		mainCamera = Camera.main;
		
		//Setting Network animator
		networkAnimator = GetComponent<NetworkAnimator> ();
		for (int i = 0; i < animatorParameterCount; i++) 
		{
			networkAnimator.SetParameterAutoSend (i, true);
		}
	}

	void Update ()
	{
		if (!isLocalPlayer) {
			return;
		}
		//Raycasting for player's destination
		RaycastingPosition ();

		//Update animation with reference to navmesh velocity
		UpdatePlayerAnimation ();

		//Update Attack	
		UpdatePlayerAttackMove ();
	}


	#region NavMesh Control
	void RaycastingPosition () 
	{
		if (Input.GetMouseButton (rightMouseClick)) 
		{
			Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition), out hit);
			if (hit.transform.gameObject.layer == enemyLayerMask)
			{
				CmdScrPlayerSetDestination (hit.point);
				enemyTransform = hit.collider.transform;
				isEnemySelected = true;
				attackCoolDown = playerAttackDelay;
			} 
			else 
			{
				CmdScrPlayerSetDestination (hit.point);
				ResetEnemySelection ();
			}
		}
	}
	#endregion

	#region Player Action
	// Using player velocity to Check Character animation
	void UpdatePlayerAnimation () 
	{
		float velocity = playerNavmeshAgent.velocity.magnitude;
		if (velocity > minRunVelocity)
		{
			playerAnimator.SetFloat ("Speed", velocity);
		}
		else 
		{
			playerAnimator.SetFloat ("Speed", velocity);
		}
	}


	void UpdatePlayerAttackMove () 
	{
		if (isEnemySelected && enemyTransform != null) 
		{
			distanceFromEnemy = Vector3.Distance (playerTransform.position, enemyTransform.position);
			AttackSelectedEnemy ();
		} 
		
		else 
		{
			ResetEnemySelection ();
		}
	}

	// Attack enemy in a range 
	void AttackSelectedEnemy () 
	{
		if (distanceFromEnemy < attackRange) 
		{
			RotatePlayerTowards (new Vector3 (enemyTransform.position.x, playerTransform.position.y, enemyTransform.position.z));
			CmdScrPlayerSetDestination (playerTransform.position);
			attackCoolDown -= Time.deltaTime;
			if (attackCoolDown < 0) 
			{
				CmdPlayerTriggerAnimation ();
				CmdFire ();
				attackCoolDown = playerAttackDelay;
			}
		}
	}																																				

	void RotatePlayerTowards (Vector3 to) 
	{
		Quaternion lookRotation =
		Quaternion.LookRotation ((to - playerTransform.position).normalized);

		//To rotate over time
		playerTransform.rotation =
		Quaternion.Slerp (playerTransform.rotation, lookRotation, Time.deltaTime * 10f);

		//To Rotate instant
		//playerTransform.rotation = _lookRotation;
	}

	public void ResetEnemySelection ()
	{
		isEnemySelected = false;
		readyToAttack = false;
		enemyTransform = null;
	}
	#endregion

	#region Network Side
	[Command]
	public void CmdScrPlayerSetDestination (Vector3 argPosition) 
	{
		//Step B, I do simple work, I not verifi a valid position in server, I only send to all clients
		RpcScrPlayerSetDestination (argPosition);
	}

	[ClientRpc]
	public void RpcScrPlayerSetDestination (Vector3 argPosition)
	{
		//Step C, only the clients move
		playerNavmeshAgent.SetDestination (argPosition);
	}

	[Command]
	public void CmdPlayerTriggerAnimation () 
	{
		RpcPlayerTriggerAnimation ();
	}

	[ClientRpc]
	public void RpcPlayerTriggerAnimation () 
	{
		playerAnimator.SetTrigger ("Fight");
	}

	// This [Command] code is called on the Client …
	// … but it is run on the Server!
	[Command]
	void CmdFire () 
	{
		// Create the Bullet from the Bullet Prefab
		var bullet = (GameObject)Instantiate (
			projectilePrefab,
			projectileSpawnPos.position,
			projectileSpawnPos.rotation);

		// Spawn the bullet on the Clients
		NetworkServer.Spawn (bullet);
	}

	public override void OnStartLocalPlayer () 
	{
		Debug.Log ("LocalPlayer");
	}

	#endregion
}
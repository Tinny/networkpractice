﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

public class EnemyBehaviour : MonoBehaviour 
{
	[SerializeField]
	private float distanceAway;

	[SerializeField]
	private Transform thisObject;

	private Transform target;

	[SerializeField]
	private NavMeshAgent navComponent;

	void Start () 
	{
		thisObject = this.transform;
	}

	void Update ()
	{
		target = NetworkingPlayerController.instRef.PlayerTransform;
		float distance = Vector3.Distance (target.position, thisObject.position);
		if (distance <5.0f) 
		{
			navComponent.SetDestination (target.position);
		} 
		else 
		{
			navComponent.SetDestination (transform.position);
		}
	}
}